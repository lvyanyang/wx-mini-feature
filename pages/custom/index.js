import service from "../../utils/service";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageIndex: 1,
    pageSize: 30,
    lastPage: false,
    moreShow: false,
    list: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadData(false);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      pageIndex: 1,
      lastPage: false,
      moreShow: false
    })
    this.loadData(true);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.lastPage == true) return;

    this.setData({
      pageIndex: this.data.pageIndex + 1,
      moreShow: true
    })

    this.loadData(false);
  },

  loadData(isRefresh) {
    service.customer.selectPageList({
      pageIndex: this.data.pageIndex,
      pageSize: this.data.pageSize
    }).then(r => {
      console.log(r);
      let list = [];
      if (r.data.rows) {
        list = r.data.rows;
      }
      if (isRefresh === false) {
        list = this.data.list.concat(list);
      }
      let lastPage = list.length == r.data.total;
      this.setData({
        list: list,
        lastPage: lastPage,
        moreShow: lastPage
      })
    }).catch(r => {
      console.error(r);
    }).finally(() => {
      if (isRefresh) {
        wx.stopPullDownRefresh();
      }
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  onClick: function () {
    wx.showToast({
      title: 'title'
    })
  }
})