const request = require('./request')
const config = require('./config')

/** 客户服务 */
const customerService = {
  /**
   * 根据主键查询单个客户档案
   * @param id {string} 客户主键
   * @return {Promise<Object>}
   */
  selectById: (id) => request.post(`/om/basicCustomer/selectById/${id}`),

  /**
   * 查询客户档案列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectPageList: (data = {}) => request.post(`/om/basicCustomer/selectPageList`, buildPageParam(data)),
}

/** 车辆服务 */
const vehicleService = {
  /**
   * 根据主键查询单个车辆档案
   * @param id {string} 车辆主键
   * @return {Promise<Object>}
   */
  selectById: (id) => request.post(`/om/basicVehicle/selectById/${id}`),

  /**
   * 查询车辆档案列表
   * @param data {Object} [data={}] 请求参数
   * @return {Promise<Object>}
   */
  selectPageList: (data = {}) => request.post(`/om/basicVehicle/selectPageList`, buildPageParam(data)),
}

function buildPageParam(data) {
  if (!data) data = {};
  return {pageSize: config.pageSize, ...data};
}

module.exports = {
  customer: customerService,
  vehicle: vehicleService
}