const envs = {
  dev: {
    baseUrl: 'https://api.xa96716.cn/api',
    appId: '21176648446192',
    debug: true,
    pageSize: 10,
    header: {
      'content-type': 'application/x-www-form-urlencoded',
      'X-Requested-With': 'AppHttpRequest',
    }
  }
}

module.exports = {
  ...envs.dev
}